import 'package:flutter/material.dart';
import '../common/text_editor.dart';
import '../common/url_appbar.dart';
import '../common/side_drawer.dart';
import '../data_wrapper.dart';

class MainScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    String currentUrl = DataWrapper.of(context).data.currentUrl;

    currentUrl = currentUrl == null ? "dontpad" : currentUrl;

    //draw main screen
    return Scaffold(
      drawer: SideDrawer(),
      appBar: UrlAppBar(Icons.folder, '/', currentUrl),
      body: TextEditor(currentUrl)
    );
  }
}