import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Data {

  String currentUrl="test";

  Data(){
    SharedPreferences.getInstance().then(( m ) {
      currentUrl = m.getString('currentUrl') ?? currentUrl;
    });
  }
}

class DataWrapper extends InheritedWidget {

  final Data data=Data();

  DataWrapper({Widget child}) : super(child: child);

  static of(BuildContext context) => context.dependOnInheritedWidgetOfExactType<DataWrapper>();

  bool updateShouldNotify(DataWrapper dataProvider) => dataProvider.data.currentUrl != data.currentUrl;
}