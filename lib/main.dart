import 'package:flutter/material.dart';
import 'main_screen/main_screen.dart';
import 'data_wrapper.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      //define the routes of our application
      initialRoute: '/',
      routes: {
        //main screen
        '/': (context) => DataWrapper(
          child: MainScreen()
        ),

        /*
        '/navigation'
        '/downloaded'
        */
      },
      title: 'Dontpad',
      theme: ThemeData(

        primarySwatch: Colors.blueGrey,
      )
    );
  }
}