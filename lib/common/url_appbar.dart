import 'package:flutter/material.dart';
import '../globals.dart' as globals;

class UrlAppBar extends StatelessWidget with PreferredSizeWidget {

  //the icon that will appear on the right
  final IconData iconData;

  //the route the icon given will lead to
  final String iconRoute;

  //current url
  final String currentUrl;

  final Size preferredSize = Size.fromHeight(globals.appbarHeight);

  UrlAppBar(this.iconData, this.iconRoute, this.currentUrl);

  @override
  Widget build(BuildContext context) {

    return AppBar(
      elevation: 10,
      centerTitle: true,
      title: Text(
          currentUrl,
          overflow: TextOverflow.ellipsis,
      ),
      actions: <Widget>[
        IconButton(
          iconSize: 40.0,
          icon: Icon(iconData),
          onPressed: () => Navigator.pushReplacementNamed(context, iconRoute),
        )
      ],
    );
  }
}