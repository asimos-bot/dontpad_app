import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:dontpad_app/globals.dart' as globals;

class TextEditor extends StatefulWidget {

  final currentUrl;

  TextEditor(this.currentUrl);

  @override
  createState() => TextEditorState(currentUrl);
}

class TextEditorState extends State<TextEditor> {

  final currentUrl;

  TextEditorState(this.currentUrl);

  final _controller=TextEditingController();

  void initState(){

    Timer.periodic(Duration(seconds: 3), (timer) async {

      //get updated page content
      _controller.value = TextEditingValue(text:await http.read(globals.dontpad + currentUrl + ".txt"));
    });
  }

  @override
  Widget build(BuildContext context) {

    return TextField(
      controller: _controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 7)
      ),
      toolbarOptions: ToolbarOptions(
        cut: false,
        copy: true,
        paste: true,
        selectAll: true
      ),
      expands: true,
      minLines: null,
      maxLines: null,
    );
  }
}